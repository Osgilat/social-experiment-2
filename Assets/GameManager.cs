﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using BehaviorDesigner;
using BehaviorDesigner.Runtime;

public class GameManager : MonoBehaviour {
	
		public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game.
		public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
		public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases.
		public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
	public Image background; 
		public GameObject m_PlayerPrefab;             // Reference to the prefab the players will control.
		public PlayerManager[] m_Players;               // A collection of managers for enabling and disabling different aspects of the tanks.



		private int m_RoundNumber;                  // Which round the game is currently on.
		private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts.
		private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends.
		private PlayerManager m_RoundWinner;
		private PlayerManager m_GameWinner;
		private static int countSaved = 0;


		private void Start()
		{
			// Create the delays so they only have to be made once.
			m_StartWait = new WaitForSeconds (m_StartDelay);
			m_EndWait = new WaitForSeconds (m_EndDelay);

			// Once the tanks have been created and the camera is using them as targets, start the game.
			StartCoroutine (GameLoop ());
		}


		// This is called from start and will run each phase of the game one after another.
		private IEnumerator GameLoop ()
		{
			// Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
			yield return StartCoroutine (RoundStarting ());

			// Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
			yield return StartCoroutine (RoundPlaying());

			// Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
			yield return StartCoroutine (RoundEnding());

			// This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
			if (m_GameWinner != null)
			{
				// If there is a game winner, restart the level.
				Application.LoadLevel (Application.loadedLevel);
			}
			else
			{
				// If there isn't a winner yet, restart this coroutine so the loop continues.
				// Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
				StartCoroutine (GameLoop ());
			}
		}


		private IEnumerator RoundStarting ()
		{

			// Increment the round number and display text showing the players what round it is.
			m_RoundNumber++;
			m_MessageText.text = "ROUND " + m_RoundNumber;
		background.CrossFadeAlpha (0, 0.1f, false);

			// Wait for the specified length of time until yielding control back to the game loop.
			yield return m_StartWait;
		}


		private IEnumerator RoundPlaying ()
		{
			// Clear the text from the screen.
			m_MessageText.text = string.Empty;
		background.CrossFadeAlpha (0, 0.1f, false);

			// While there is not one tank left...
		while (countSaved != 4)
			{
				// ... return on the next frame.
				yield return null;
			}
		}
		
	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
		countSaved++;
			Debug.Log ("GAME MANAGER TRIGGER" + countSaved);
		}
	}

		private IEnumerator RoundEnding ()
		{

//			// Clear the winner from the previous round.
//			m_RoundWinner = null;
//
//			// See if there is a winner now the round is over.
//			m_RoundWinner = GetRoundWinner ();
//
//			// If there is a winner, increment their score.
//			if (m_RoundWinner != null)
//				m_RoundWinner.m_Wins++;
//
//			// Now the winner's score has been incremented, see if someone has one the game.
//			m_GameWinner = GetGameWinner ();


		yield return m_StartWait;

		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		GameObject[] spawns = GameObject.FindGameObjectsWithTag("SpawnPoint");

		for (int i = 0; i < players.Length; i++) {
			Vector3 spawnPoint = spawns[Random.Range(0,spawns.Length)].transform.position;
			players [i].GetComponent<UseTeleport> ().Cmd_TransformPlayer (players[i], spawnPoint);
		}

	

		countSaved = 0;

			//			playerA = playerB;




			// Get a message based on the scores and whether or not there is a game winner and display it.
			string message = EndMessage ();
			m_MessageText.text = message;
		background.CrossFadeAlpha (100, 0.1f, false);

			// Wait for the specified length of time until yielding control back to the game loop.
			yield return m_EndWait;

		var behaviorTree = GameObject.Find("PlayerMod_C(Clone)").GetComponent<BehaviorTree>();
		behaviorTree.SendEvent<object> ("Restart", 1);
		}


		// This is used to check if there is one or fewer tanks remaining and thus the round should end.
//		private bool OneTankLeft()
//		{
//			// Start the count of tanks left at zero.
//			int numTanksLeft = 0;
//
//			// Go through all the tanks...
//		for (int i = 0; i < m_Players.Length; i++)
//			{
//				// ... and if they are active, increment the counter.
//			if (m_Players[i].m_Instance.activeSelf)
//					numTanksLeft++;
//			}
//
//			// If there are one or fewer tanks remaining return true, otherwise return false.
//			return numTanksLeft <= 1;
//		}


//		// This function is to find out if there is a winner of the round.
//		// This function is called with the assumption that 1 or fewer tanks are currently active.
//		private PlayerManager GetRoundWinner()
//		{
//			// Go through all the tanks...
//		for (int i = 0; i < m_Players.Length; i++)
//			{
//				// ... and if one of them is active, it is the winner so return it.
//			if (m_Players[i].m_Instance.activeSelf)
//				return m_Players[i];
//			}
//
//			// If none of the tanks are active it is a draw so return null.
//			return null;
//		}
//
//
//		// This function is to find out if there is a winner of the game.
//		private PlayerManager GetGameWinner()
//		{
//			// Go through all the tanks...
//		for (int i = 0; i < m_Players.Length; i++)
//			{
//				// ... and if one of them has enough rounds to win the game, return it.
//			if (m_Players[i].m_Wins == m_NumRoundsToWin)
//				return m_Players[i];
//			}
//
//			// If no tanks have enough rounds to win, return null.
//			return null;
//		}


		// Returns a string message to display at the end of each round.
		private string EndMessage()
		{
//			// By default when a round ends there are no winners so the default end message is a draw.
			string message = "ROUND END!";
//
//			// If there is a winner then change the message to reflect that.
//			if (m_RoundWinner != null)
//				message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";
//
//			// Add some line breaks after the initial message.
//			message += "\n\n\n\n";
//
//			// Go through all the tanks and add each of their scores to the message.
//		for (int i = 0; i < m_Players.Length; i++)
//			{
//			message += m_Players[i].m_ColoredPlayerText + ": " + m_Players[i].m_Wins + " WINS\n";
//			}
//
//			// If there is a game winner, change the entire message to reflect that.
//			if (m_GameWinner != null)
//				message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";
//
			return message;
		}


//		// This function is used to turn all the tanks back on and reset their positions and properties.
//		private void ResetAllTanks()
//		{
//		for (int i = 0; i < m_Players.Length; i++)
//			{
//			m_Players[i].Reset();
//			}
//		}
}
