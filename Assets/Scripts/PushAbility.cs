﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PushAbility : NetworkBehaviour {

	private static string pushed = "not pushed";
	public float moveSpeed = 1000000;
	public float rotationSpeed;
	private Vector3 targetDir;
	private Button push;
	public GameObject pushCube;
	public Transform bulletSpawn;
	public AudioSource kickSound;

	private Animator anim;

	void Start(){
		anim = GetComponentInChildren<Animator> ();
	}


	public void ClickToPush(){
		pushed = "";
	}


	void Update(){

		if (!isLocalPlayer) {
			return;
		}

		Transform player;

		if (Input.GetMouseButton (0) && pushed == "") {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit)) {
				if (hit.transform.CompareTag ("Player")) {
					player = hit.transform;
					targetDir = player.position - this.transform.position;
					float step = rotationSpeed * Time.deltaTime;
			
					Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
					Debug.DrawRay (transform.position, newDir, Color.red);
					transform.rotation = Quaternion.LookRotation (newDir);

					Cmd_CollidersIdentify(this.transform.position, 5.0f);

				}
			}
		}

	}

	public void AI_AttackPlayer(GameObject ai_target){
		
			Transform player = ai_target.transform;
			targetDir = player.position - this.transform.position;
			float step = rotationSpeed * Time.deltaTime;

			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
			Debug.DrawRay (transform.position, newDir, Color.red);
			transform.rotation = Quaternion.LookRotation (newDir);
			Cmd_CollidersIdentify(this.transform.position, 5.0f);

	}


	[Command]
		 void Cmd_CollidersIdentify(Vector3 center, float radius) {
         Collider[] hitColliders = Physics.OverlapSphere(center, radius);
         int i = 0;
         while (i < hitColliders.Length) {
             if(hitColliders[i].tag =="Player" && hitColliders[i].gameObject != gameObject)
                 {
				GameObject player = hitColliders[i].gameObject;
				anim.SetTrigger ("TriggerAnger");
				kickSound.Play ();
				Rpc_Move (player);
			   pushed = "not pushed";
                 }
             i++;
             }
         }

	[ClientRpc]
	void Rpc_Move(GameObject player){
		Debug.Log ("Kick from " + this.name + " to a " + player.name);
		player.GetComponent<NavMeshAgent>().velocity = transform.forward*15;
		player.GetComponent<NavMeshAgent>().ResetPath();
	}
}
