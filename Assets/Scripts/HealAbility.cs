﻿using UnityEngine;
using System.Collections;

public class HealAbility : MonoBehaviour {

	public GameObject cube;

	//public Texture2D ab1;
	//public Texture2D ab1CD;
	float healTimer = 0;
	public float healCDTime;

	void OnGUI(){

		healTimer -= Time.deltaTime;
		bool healKey = Input.GetKeyDown (KeyCode.W);

		if (healTimer <= 0) {
			GUI.Button(new Rect(935, 150, 340,125), "Heal");

			if (healKey) {
				Heal ();
			}

		} else {
			GUI.Button(new Rect(935, 150, 340,125), "HealCD");
		}
	}

	void Heal(){
		Instantiate (cube, Vector3.zero, Quaternion.identity);
		healTimer = healCDTime;
	}
}
