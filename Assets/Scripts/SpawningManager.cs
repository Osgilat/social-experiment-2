﻿using UnityEngine;
using UnityEngine.Networking;

public class SpawningManager : MonoBehaviour {

	public GameObject platformPrefab1;
	public GameObject platformPrefab2;

	public void Spawn(){
		GameObject platform1 = (GameObject) Instantiate(platformPrefab1, transform.position, transform.rotation);
		GameObject platform2 = (GameObject) Instantiate(platformPrefab1, transform.position, transform.rotation);
		NetworkServer.Spawn(platform1);
		NetworkServer.Spawn(platform2);
	}
}
