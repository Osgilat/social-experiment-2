﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class UseTeleport : NetworkBehaviour {
	
	private static GameObject thisTeleport;
	private static GameObject otherTeleport;
	public GameObject greetButton;
	public GameObject kickButton;
	public GameObject activateButton;
	public GameObject takeOffButton;
	public GameObject saveButton;
	public GameObject escapeButton;
	public GameObject canvas;
	public GameObject pos_1;
	public GameObject pos_2;
	public AudioSource tpSound;



	private static String active = "not active";
	private static String isTeleported = "not teleported"; 
	public static String saveOther = "not saved";
	[SyncVar]
	private GameObject objectID;



	private NetworkIdentity objNetId;

	// Update is called once per frame

	void Start(){
		Transform thisCanvas = (Instantiate (canvas)).transform;
		greetButton =  thisCanvas.GetChild (0).gameObject; 
		kickButton =  thisCanvas.GetChild (1).gameObject; 
		activateButton = thisCanvas.GetChild (2).gameObject; 
		takeOffButton = thisCanvas.GetChild (3).gameObject; 
		saveButton = thisCanvas.GetChild (4).gameObject; 
		escapeButton = thisCanvas.GetChild (5).gameObject; 

		activateButton.SetActive (false);
		takeOffButton.SetActive (false);
		saveButton.SetActive (false);
		escapeButton.SetActive (false);
	}

	void Update () {
		
		if (isLocalPlayer) {

			if (isTeleported == null) {
				Cmd_TransformPlayer (this.transform.gameObject, pos_1.transform.position);
				greetButton.SetActive (false);
				kickButton.SetActive (false);
				saveButton.SetActive (true);
				escapeButton.SetActive (true);
				if (saveOther == null) {
					if (Input.GetMouseButton (0)) {
						RaycastHit hit;
						Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

						if (Physics.Raycast (ray, out hit)) {
							if (hit.transform.CompareTag ("Player")) {
								Cmd_TransformPlayer (hit.transform.gameObject, pos_2.transform.position);
								}
						}
					}
				}
			}

			if (active == null) {
				active = "not active";
				objectID = otherTeleport;

				CmdActivateOtherTeleport (objectID);
			}
		}

	}

	[Command]
	void CmdActivateOtherTeleport (GameObject obj){
		objNetId = obj.GetComponent<NetworkIdentity> ();
		objNetId.AssignClientAuthority (connectionToClient);
		Rpc_ActivateOtherTeleport (obj);
		objNetId.RemoveClientAuthority (connectionToClient);
	}

	[ClientRpc]
	void Rpc_ActivateOtherTeleport(GameObject obj){
		ParticleSystem tp = obj.GetComponent<ParticleSystem> ();
		if (tp.isPlaying) {
			tp.Stop ();

		} else if (tp.isStopped) {
			tp.Play ();
		}
	}


	[Command]
	void CmdStopOtherTeleport (GameObject obj){
		objNetId = obj.GetComponent<NetworkIdentity> ();
		objNetId.AssignClientAuthority (connectionToClient);
		Rpc_StopOtherTeleport (obj);
		objNetId.RemoveClientAuthority (connectionToClient);
	}

	[ClientRpc]
	void Rpc_StopOtherTeleport(GameObject obj){
		ParticleSystem tp = obj.GetComponent<ParticleSystem> ();
			tp.Stop ();
	}

	[Command]
	public void Cmd_TransformPlayer(GameObject obj, Vector3 position){
		Rpc_TransformPlayer (obj, position);

		
	}

	[ClientRpc]
	public void Rpc_TransformPlayer(GameObject obj, Vector3 position){
		obj.GetComponent<NavMeshAgent>().Warp(position);
	}

	void OnTriggerEnter(Collider other)
	{
		
		if(isLocalPlayer)
		if(other.gameObject.tag == "Platform_1" || other.gameObject.tag == "Platform_2"){
			activateButton.SetActive(true);
		}

		if (other.gameObject.tag == "Platform_1") {
			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_2");
			Debug.Log(this.name +" entering " + thisTeleport);
		
		} else if (other.gameObject.tag == "Platform_2") {

			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_1");
			Debug.Log(this.name +" entering " + thisTeleport);
		}
	}


	void OnTriggerStay(Collider other)
	{
		if(isLocalPlayer)
		if (other.gameObject.tag == "Platform_1"){
			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_2");
			if (thisTeleport.GetComponent<ParticleSystem> ().isPlaying) {
				takeOffButton.SetActive (true);
			} else if (thisTeleport.GetComponent<ParticleSystem> ().isStopped) {
				takeOffButton.SetActive (false);
			}

		}	else if( other.gameObject.tag == "Platform_2"){

			thisTeleport = other.gameObject;
			otherTeleport = GameObject.FindGameObjectWithTag ("Platform_1");

			if (thisTeleport.GetComponent<ParticleSystem> ().isPlaying) {
				takeOffButton.SetActive (true);
			} else if (thisTeleport.GetComponent<ParticleSystem> ().isStopped) {
				takeOffButton.SetActive (false);
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(isLocalPlayer)
		if (other.gameObject.tag == "Platform_1" || other.gameObject.tag == "Platform_2") {
			takeOffButton.SetActive (false);
			activateButton.SetActive (false);

			Debug.Log (this.name + "exiting" + other.name);

			CmdStopOtherTeleport (otherTeleport);

			thisTeleport = null;
			otherTeleport = null;
		}
	}

	public void ActivateTeleportOnClick(){

		active = null;
//		tpSound.Play ();
		Debug.Log (this.name + " activated other teleport");
		}

	public void AI_ActivateTeleport(){
		active = null;
//		tpSound.Play ();
		Debug.Log (this.name + " activated other teleport");
	}

	public void ActivateTakeOffButton(){
		isTeleported = null;
		Debug.Log ("Activated TAKE OFF by" +  this.gameObject.name);
	}

	public void AI_ActivateTakeOffButton(){
		Debug.Log (thisTeleport);
		Debug.Log (thisTeleport.GetComponent<ParticleSystem> ().isPlaying);
		if (thisTeleport.GetComponent<ParticleSystem> ().isPlaying || otherTeleport.GetComponent<ParticleSystem> ().isPlaying) {
			Cmd_TransformPlayer (this.gameObject, pos_1.transform.position);
			Debug.Log ("Activated TAKE OFF by" +  this.gameObject.name);
		}
	}


	public void AI_ActivateSaveButton(GameObject playerToSave){
		Debug.Log ("Player to save = " + playerToSave.name);
		GameObject player = GameObject.Find (playerToSave.name + "(Clone)");
		Cmd_TransformPlayer (player, pos_2.transform.position);
	}

	public void ActivateSaveButton(){
		saveOther = null;
	}

}
	


