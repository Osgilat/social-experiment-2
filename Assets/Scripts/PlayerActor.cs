﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerActor : NetworkBehaviour
{

    [SyncVar]
    public float speed;
    public float rotationSpeed;
	// public GameObject bulletPrefab = null;
	// public Transform bulletSpawn = null;
	NavMeshAgent agent;
	public AudioSource hello_A_Sound;
	public AudioSource hello_B_Sound;
	public AudioSource hello_C_Sound;
	private Animator anim;
	public static string sayHello = null;
//    private Rigidbody rb;
//	[SyncVar]
//	Queue queue = new Queue();


    void Start()
    {
		
//        rb = GetComponent<Rigidbody>();
		// ClientScene.RegisterPrefab (bulletPrefab);
		agent = GetComponent<NavMeshAgent>();
		anim = GetComponentInChildren<Animator> ();

    }

    void FixedUpdate()
    {
		if (!isLocalPlayer) {
			return;
		}

		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
				if (hit.transform.CompareTag ("Ground") || hit.transform.CompareTag ("Platform_1")
				    || hit.transform.CompareTag ("Platform_2")) {
					Debug.Log (this.name + " movement to a point = " + hit.point);
					agent.destination = hit.point;
				} 
				if(sayHello != null && hit.transform.CompareTag ("Player")){
					
					Vector3 newDir = Vector3.RotateTowards (transform.forward, hit.transform.position - this.transform.position
						, 1000 * Time.deltaTime, 0.0F);
					transform.rotation = Quaternion.LookRotation (newDir);
//					anim.SetBool ("Scared", true);
//					Animate ();
//					anim.SetBool ("Scared", false);
					anim.SetTrigger("SayHello");
					if (hit.transform.name == "PlayerMod_A(Clone)" && this.name != "PlayerMod_A(Clone)") {
						hello_A_Sound.Play ();
						Debug.Log ("Hello to Player A");
					} else if (hit.transform.name == "PlayerMod_B(Clone)" && this.name != "PlayerMod_B(Clone)") {
						hello_B_Sound.Play ();
						Debug.Log ("Hello to Player B");
					} else if (hit.transform.name == "PlayerMod_C(Clone)" && this.name != "PlayerMod_C(Clone)") {
						hello_C_Sound.Play ();
						Debug.Log ("Hello to Player C");
					}
					sayHello = null;
				}
			}
		}
    }

	IEnumerator Animate(){
		
		yield return new WaitForSeconds (1.0f);

	}

	public void OnGreetHello(){
		sayHello = "sayHello";
	}

	public void AI_GreetPlayer(GameObject ai_target){

		Transform player = ai_target.transform;
		Vector3 targetDir = player.position - this.transform.position;
		float step = rotationSpeed * Time.deltaTime;

		Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
		Debug.DrawRay (transform.position, newDir, Color.red);
		transform.rotation = Quaternion.LookRotation (newDir);

		anim.SetTrigger("SayHello");

		if (player.name == "PlayerMod_A" && this.name != "PlayerMod_A(Clone)") {
			Debug.Log ("Hello to Player A from Virtual Actor");
			hello_A_Sound.Play ();
		} else if (player.name == "PlayerMod_B" && this.name != "PlayerMod_B(Clone)") {
			hello_B_Sound.Play ();
			Debug.Log ("Hello to Player B from Virtual Actor");
		} else if (player.name == "PlayerMod_C" && this.name != "PlayerMod_C(Clone)") {
			hello_C_Sound.Play ();
			Debug.Log ("Hello to Player C from Virtual Actor");
		}

		sayHello = null;

	}
		
}