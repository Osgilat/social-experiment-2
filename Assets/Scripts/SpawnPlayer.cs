﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class SpawnPlayer : NetworkBehaviour {

	/// <summary>
	/// THIS SCRIPT IS ATTACHED TO THE PLAYER GAMEOBJECT THAT IS SPAWNED WHEN THE PLAYER CONNECTS TO A GAME.
	/// </summary>
	///
	public GameObject troopPrefab;

	void Update()
	{
		if(Input.GetKey(KeyCode.Space))
			CmdRequestTroopSpawn(); //THIS SENDS A REQUEST TO THE SERVER TO SPAWN A TROOP.
	}

	[Command]    //LIKE WITH ANY COMMAND THIS CODE IS ONLY EXECUTED ON THE SERVER.
	public void CmdRequestTroopSpawn()
	{
		GameObject troop = Instantiate(troopPrefab, transform.position, Quaternion.identity) as GameObject; //SpawnWithClientAuthority WORKS JUST LIKE NetworkServer.Spawn ...THE
		//GAMEOBJECT MUST BE INSTANTIATED FIRST.

		NetworkServer.SpawnWithClientAuthority(troop, gameObject); //THIS WILL SPAWN THE troop THAT WAS CREATED ABOVE AND GIVE AUTHORITY TO THIS PLAYER. THIS PLAYER (GAMEOBJECT) MUST
		//HAVE A NetworkIdentity ON IT WITH Local Player Authority CHECKED.
	}
}
